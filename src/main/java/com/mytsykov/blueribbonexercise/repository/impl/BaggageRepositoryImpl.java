package com.mytsykov.blueribbonexercise.repository.impl;

import com.mytsykov.blueribbonexercise.data.DataSource;
import com.mytsykov.blueribbonexercise.domain.Baggage;
import com.mytsykov.blueribbonexercise.repository.BaggageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BaggageRepositoryImpl implements BaggageRepository {

    private final DataSource dataSource;

    @Autowired
    public BaggageRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Baggage save(Baggage baggage) {
        return dataSource.getBaggages().put(baggage.getId(), baggage);
    }

    @Override
    public Baggage findById(String id) {
        return dataSource.getBaggages().get(id);
    }
}
