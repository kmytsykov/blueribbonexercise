package com.mytsykov.blueribbonexercise.repository.impl;

import com.mytsykov.blueribbonexercise.data.DataSource;
import com.mytsykov.blueribbonexercise.domain.Ticket;
import com.mytsykov.blueribbonexercise.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TicketRepositoryImpl implements TicketRepository {

    private final DataSource dataSource;

    @Autowired
    public TicketRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Ticket findOneById(Integer id) {
        return dataSource.getTickets().get(id);
    }

    @Override
    public Ticket findByDestination(Integer destinationId) {
        return dataSource.getTickets()
                .values()
                .stream()
                .filter(ticket -> destinationId.equals(ticket.getDestination().getId()))
                .findFirst()
                .orElse(null);
    }
}
