package com.mytsykov.blueribbonexercise.servcie;

import com.mytsykov.blueribbonexercise.dto.DiscountPrice;

public interface DiscountService {
    DiscountPrice getPrice(Integer couponId, Double price);
}
