package com.mytsykov.blueribbonexercise.repository.impl;

import com.mytsykov.blueribbonexercise.data.DataSource;
import com.mytsykov.blueribbonexercise.domain.Coupon;
import com.mytsykov.blueribbonexercise.repository.CouponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CouponRepositoryImpl implements CouponRepository {

    @Autowired
    private DataSource dataSource;

    @Override
    public Coupon findOneById(Integer id) {
        return dataSource.getCoupons().get(id);
    }
}
