package com.mytsykov.blueribbonexercise.repository;

import com.mytsykov.blueribbonexercise.domain.Coupon;

public interface CouponRepository {
    Coupon findOneById(Integer id);
}
