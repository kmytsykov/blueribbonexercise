package com.mytsykov.blueribbonexercise.servcie;

import com.mytsykov.blueribbonexercise.domain.Ticket;

public interface TicketService {
    boolean isValid(Integer ticketId);
    Ticket findByDestination(Integer destinationId);
}
