package com.mytsykov.blueribbonexercise.data;

import com.mytsykov.blueribbonexercise.domain.Coupon;
import com.mytsykov.blueribbonexercise.domain.Destination;
import com.mytsykov.blueribbonexercise.domain.Ticket;
import com.mytsykov.blueribbonexercise.domain.Baggage;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@Repository
@Scope
@Getter
public class DataSource {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataSource.class);

    private Map<String, Baggage> baggages;

    private Map<Integer, Ticket> tickets;

    private Map<Integer, Coupon> coupons;

    public DataSource() {
        initBaggages();
        generateMockCoupons();
        generateMockTickets();
    }

    private void initBaggages() {
        baggages = new HashMap<>();
    }

    private void generateMockCoupons() {
        LOGGER.info("###############################");
        LOGGER.info("INIT COUPONS");

        coupons = new HashMap<>();
        for (int i = 0; i < 3; i++) {
            double discount = ThreadLocalRandom.current().nextInt(1, 8) * 10;
            Coupon coupon = createCoupon(i, discount);
            coupons.put(i, coupon);
            LOGGER.info("Coupon {}: {}", i, coupon);
        }
        LOGGER.info("###############################");
    }

    private void generateMockTickets() {
        LOGGER.info("###############################");
        LOGGER.info("INIT TICKETS");

        tickets = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            Ticket ticket = createTicket(i);
            tickets.put(i, ticket);
            LOGGER.info("Ticket {}: {}", i, ticket);
        }
        LOGGER.info("###############################");
    }

    private Coupon createCoupon(int id, Double discount) {
        return Coupon.builder()
                .id(id)
                .discount(discount)
                .build();
    }

    private Ticket createTicket(int id) {
        String destinationPrefix = "destination";
        return Ticket.builder()
                .id(id)
                .destination(new Destination(id, destinationPrefix + id))
                .build();
    }
}
