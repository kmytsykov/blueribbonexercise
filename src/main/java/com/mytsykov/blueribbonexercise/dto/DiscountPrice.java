package com.mytsykov.blueribbonexercise.dto;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode
public class DiscountPrice {
    private Double price;
    private Boolean couponValid;
}
