package com.mytsykov.blueribbonexercise.repository;

import com.mytsykov.blueribbonexercise.domain.Baggage;

public interface BaggageRepository {

    Baggage save(Baggage baggage);

    Baggage findById(String id);
}
