package com.mytsykov.blueribbonexercise.config;

import com.mytsykov.blueribbonexercise.interceptor.CacheInterceptor;
import com.mytsykov.blueribbonexercise.interceptor.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppConfig implements WebMvcConfigurer {

    private final RequestInterceptor requestInterceptor;
    private final CacheInterceptor cacheInterceptor;

    @Autowired
    public AppConfig(RequestInterceptor requestInterceptor,
                     CacheInterceptor cacheInterceptor) {
        this.requestInterceptor = requestInterceptor;
        this.cacheInterceptor = cacheInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestInterceptor).addPathPatterns("/**");
        registry.addInterceptor(cacheInterceptor).addPathPatterns("/**");
    }
}
