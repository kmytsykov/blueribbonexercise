package com.mytsykov.blueribbonexercise.servcie.impl;

import com.mytsykov.blueribbonexercise.domain.Baggage;
import com.mytsykov.blueribbonexercise.domain.Ticket;
import com.mytsykov.blueribbonexercise.dto.CheckInResultDto;
import com.mytsykov.blueribbonexercise.repository.BaggageRepository;
import com.mytsykov.blueribbonexercise.servcie.CheckInService;
import com.mytsykov.blueribbonexercise.servcie.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@Service
public class CheckInServiceImpl implements CheckInService {

    private final TicketService ticketService;
    private final BaggageRepository baggageRepository;

    @Autowired
    public CheckInServiceImpl(TicketService ticketService,
                              BaggageRepository baggageRepository) {
        this.ticketService = ticketService;
        this.baggageRepository = baggageRepository;
    }

    @Override
    public CheckInResultDto checkIn(Integer destinationId, String baggageId) {
        //Check if already checked in
        if (baggageRepository.findById(baggageId) != null) {
            return new CheckInResultDto(true);
        }

        Ticket ticket = ticketService.findByDestination(destinationId);
        if (ticket != null) {
            Baggage baggage = Baggage.builder()
                    .id(baggageId)
                    .ticket(ticket)
                    .build();

            baggageRepository.save(baggage);
            return new CheckInResultDto(true);
        }

        return new CheckInResultDto(false);
    }
}
