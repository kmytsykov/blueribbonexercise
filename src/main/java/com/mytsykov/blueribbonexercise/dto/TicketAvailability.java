package com.mytsykov.blueribbonexercise.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TicketAvailability {
    private boolean available;
}
