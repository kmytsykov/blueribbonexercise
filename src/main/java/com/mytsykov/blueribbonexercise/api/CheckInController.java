package com.mytsykov.blueribbonexercise.api;

import com.mytsykov.blueribbonexercise.dto.CheckInDto;
import com.mytsykov.blueribbonexercise.dto.CheckInResultDto;
import com.mytsykov.blueribbonexercise.servcie.CheckInService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/check-in")
public class CheckInController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CheckInController.class);

    private final CheckInService checkInService;

    @Autowired
    public CheckInController(CheckInService checkInService) {
        this.checkInService = checkInService;
    }

    @PostMapping
    public CheckInResultDto checkIn(@RequestBody CheckInDto checkInData) {
        LOGGER.debug("New request");
        return checkInService.checkIn(checkInData.getDestinationId(), checkInData.getBaggageId());
    }
}
