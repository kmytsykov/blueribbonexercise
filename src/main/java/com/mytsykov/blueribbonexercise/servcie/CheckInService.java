package com.mytsykov.blueribbonexercise.servcie;

import com.mytsykov.blueribbonexercise.dto.CheckInResultDto;

public interface CheckInService {
    CheckInResultDto checkIn(Integer ticketId, String baggageId);
}
