package com.mytsykov.blueribbonexercise.cache;

import org.apache.commons.collections4.map.PassiveExpiringMap;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class InMemoryCache {

    private Map<String, Object> cache = new PassiveExpiringMap<>(1, TimeUnit.MINUTES);

    public Object get(String request) {
        return cache.get(request);
    }

    public void doCache(String request, Object response) {
        cache.put(request, response);
    }
}
