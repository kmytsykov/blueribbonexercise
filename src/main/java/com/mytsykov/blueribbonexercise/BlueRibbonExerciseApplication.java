package com.mytsykov.blueribbonexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlueRibbonExerciseApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlueRibbonExerciseApplication.class, args);
    }
}
