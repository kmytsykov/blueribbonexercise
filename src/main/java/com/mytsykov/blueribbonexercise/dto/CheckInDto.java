package com.mytsykov.blueribbonexercise.dto;

import lombok.Data;

@Data
public class CheckInDto {
    private Integer destinationId;
    private String baggageId;
}
