package com.mytsykov.blueribbonexercise.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Coupon {
    private Integer id;
    private double discount;
}
