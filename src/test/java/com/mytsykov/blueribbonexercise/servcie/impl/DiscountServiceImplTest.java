package com.mytsykov.blueribbonexercise.servcie.impl;

import com.mytsykov.blueribbonexercise.domain.Coupon;
import com.mytsykov.blueribbonexercise.dto.DiscountPrice;
import com.mytsykov.blueribbonexercise.repository.CouponRepository;
import com.mytsykov.blueribbonexercise.servcie.DiscountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DiscountServiceImplTest {

    private DiscountService discountService;
    private CouponRepository couponRepository;

    @BeforeEach
    void setUp() {
        couponRepository = Mockito.mock(CouponRepository.class);
        discountService = new DiscountServiceImpl(couponRepository);
    }

    @Test
    void getPriceIfValidCouponProvided() {
        Double fullPrice = 100d;

        Coupon coupon0 = Coupon.builder().id(0).discount(0).build();
        DiscountPrice discountPrice0 = DiscountPrice.builder().price(100.0).couponValid(true).build();

        Coupon coupon60 = Coupon.builder().id(1).discount(60).build();
        DiscountPrice discountPrice60 = DiscountPrice.builder().price(40.0).couponValid(true).build();


        Mockito.when(couponRepository.findOneById(0)).thenReturn(coupon0);
        Mockito.when(couponRepository.findOneById(1)).thenReturn(coupon60);

        assertEquals(discountPrice0, discountService.getPrice(0, fullPrice));
        assertEquals(discountPrice60, discountService.getPrice(1, fullPrice));

    }

    @Test
    void getPriceIfInvalidCouponProvided() {
        Mockito.when(couponRepository.findOneById(0)).thenReturn(null);
        DiscountPrice discountPrice = DiscountPrice.builder().price(100.0).couponValid(false).build();

        assertEquals(discountPrice, discountService.getPrice(0, 100d));
    }

    @Test
    void getPriceIfCouponNotProvided() {
        DiscountPrice discountPrice = DiscountPrice.builder().price(100.0).couponValid(null).build();

        assertEquals(discountPrice, discountService.getPrice(null, 100d));
    }
}