package com.mytsykov.blueribbonexercise.api;

import com.mytsykov.blueribbonexercise.dto.DiscountPrice;
import com.mytsykov.blueribbonexercise.servcie.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/price")
public class PriceController {

    private final DiscountService discountService;

    @Autowired
    public PriceController(DiscountService discountService) {
        this.discountService = discountService;
    }

    @GetMapping
    public DiscountPrice getDiscountPrice(@RequestParam(required = false) Integer couponId,
                                          @RequestParam Double price) {
        return discountService.getPrice(couponId, price);
    }
}
