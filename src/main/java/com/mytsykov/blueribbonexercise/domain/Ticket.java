package com.mytsykov.blueribbonexercise.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Ticket {
    private Integer id;
    private Destination destination;
}
