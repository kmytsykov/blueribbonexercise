package com.mytsykov.blueribbonexercise.servcie.impl;

import com.mytsykov.blueribbonexercise.domain.Ticket;
import com.mytsykov.blueribbonexercise.repository.TicketRepository;
import com.mytsykov.blueribbonexercise.servcie.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public boolean isValid(Integer ticketId) {
        return ticketRepository.findOneById(ticketId) == null ? false : true;
    }

    @Override
    public Ticket findByDestination(Integer destinationId) {
        return ticketRepository.findByDestination(destinationId);
    }
}
