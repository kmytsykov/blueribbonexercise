package com.mytsykov.blueribbonexercise.servcie.impl;

import com.mytsykov.blueribbonexercise.domain.Coupon;
import com.mytsykov.blueribbonexercise.dto.DiscountPrice;
import com.mytsykov.blueribbonexercise.repository.CouponRepository;
import com.mytsykov.blueribbonexercise.servcie.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DiscountServiceImpl implements DiscountService {

    private final CouponRepository couponRepository;

    @Autowired
    public DiscountServiceImpl(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    @Override
    public DiscountPrice getPrice(Integer couponId, Double price) {
        if (couponId == null)
            return DiscountPrice.builder().price(price).build();

        Coupon coupon = couponRepository.findOneById(couponId);

        return coupon == null
                ? DiscountPrice.builder().couponValid(false).price(price).build()
                : DiscountPrice.builder().couponValid(true).price(price * (1 - coupon.getDiscount() / 100)).build();

    }
}
