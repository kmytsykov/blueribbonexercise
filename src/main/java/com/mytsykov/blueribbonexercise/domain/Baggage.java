package com.mytsykov.blueribbonexercise.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Baggage {

    // Maybe it's a mistake, but the baggage ID in the task text is defined by String
    private String id;
    private Ticket ticket;
}
