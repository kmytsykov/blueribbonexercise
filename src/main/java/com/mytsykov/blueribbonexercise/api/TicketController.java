package com.mytsykov.blueribbonexercise.api;

import com.mytsykov.blueribbonexercise.dto.TicketAvailability;
import com.mytsykov.blueribbonexercise.servcie.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tickets")
public class TicketController {

    private final TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @GetMapping("/{ticketId}")
    public TicketAvailability checkTicketAvailability(@PathVariable Integer ticketId) {
        return TicketAvailability.builder().available(ticketService.isValid(ticketId)).build();
    }
}
