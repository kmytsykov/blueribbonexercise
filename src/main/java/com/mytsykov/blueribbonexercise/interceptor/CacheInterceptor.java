package com.mytsykov.blueribbonexercise.interceptor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mytsykov.blueribbonexercise.cache.InMemoryCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class CacheInterceptor extends HandlerInterceptorAdapter {

    private final InMemoryCache inMemoryCache;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public CacheInterceptor(InMemoryCache inMemoryCache) {
        this.inMemoryCache = inMemoryCache;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (HttpMethod.GET.toString().equals(request.getMethod())) {
            Object cachedResponse = inMemoryCache.get(request.getRequestURL().toString() + "?" + request.getQueryString());
            if (cachedResponse != null) {
                response.setStatus(200);
                response.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
                objectMapper.writeValue(response.getWriter(), cachedResponse);
                return false;
            }
        }
        return true;
    }
}
