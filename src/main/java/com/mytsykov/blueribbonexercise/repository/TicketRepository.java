package com.mytsykov.blueribbonexercise.repository;

import com.mytsykov.blueribbonexercise.domain.Ticket;

public interface TicketRepository {
    Ticket findOneById(Integer id);
    Ticket findByDestination(Integer destinationId);
}
